import java.io.*;
import java.net.*;

public class ConnectGoogle {
    public static void main(String[] args) {
        int iter;
        try {
            URL url = new URL("http://www.google.com");
            URLConnection connect = url.openConnection();
            InputStream in = connect.getInputStream();

            while ((iter = in.read()) != -1) {
                System.out.print((char) iter);
            }
        } catch (Exception e) {
            System.out.println(e);
        }
    }
}