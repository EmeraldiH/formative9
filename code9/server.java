import java.net.*;
import java.io.*;
public class Server{
    private static final int PORT = 8000;
    public static void main(String[] args) {
        try(ServerSocket serverSocket = new ServerSocket(PORT)){
            while(true){
                Socket socket = serverSocket.accept();
                DataInputStream in = new DataInputStream(socket.getInputStream());
                DataOutputStream out = new DataOutputStream(socket.getOutputStream());
                String msg = in.readUTF();
                out.writeUTF(msg);
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}